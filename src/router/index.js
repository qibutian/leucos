import Vue from "vue";
import Router from "vue-router";
// 通讯录
import address from "@/views/address/address";
import address_secondary from "@/views/address/address_secondary";
import address_particulars from "@/views/address/address_particulars";
import message from "@/views/message";
import flock from "@/views/flock";
// 我的
import main from "@/views/main/main";
import main_exclusive from "@/views/main/main_exclusive";
import main_detail from "@/views/main/main_detail";
import main_start from "@/views/main/main_start";
import main_news from "@/views/main/main_news";
import main_wallet from "@/views/main/main_wallet";
import main_personagea from "@/views/main/main_personagea";
import main_personageb from "@/views/main/main_personageb";
import main_withdraw from "@/views/main/main_withdraw";
// import secondary from "@/views/secondary";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      redirect: "address"
    },
    {
      path: "/address",
      name: "address",
      component: address
    },
    {
      path: "/address_secondary",
      name: "address_secondary",
      component: address_secondary
    },
    {
      path: "/address_particulars",
      name: "address_particulars",
      component: address_particulars
    },
    {
      path: "/message",
      name: "message",
      component: message
    },
    {
      path: "/flock",
      name: "flock",
      component: flock
    },
    // 我的
    {
      path: "/main",
      name: "main",
      component: main
    },
    {
      path: "/main_exclusive",
      name: "main_exclusive",
      component: main_exclusive
    },
    {
      path: "/main_start",
      name: "main_start",
      component: main_start
    },
    {
      path: "/main_detail",
      name: "main_detail",
      component: main_detail
    },
    {
      path: "/main_wallet",
      name: "main_wallet",
      component: main_wallet
    },
    {
      path: "/main_news",
      name: "main_news",
      component: main_news
    },
    {
      path: "/main_personagea",
      name: "main_personagea",
      component: main_personagea
    },
    {
      path: '/main_personageb',
      name: 'main_personageb',
      component:main_personageb
    },
    {
      path: '/main_withdraw',
      name: 'main_withdraw',
      component:main_withdraw
    },
    // {
    //   path: '/',
    //   name: '',
    //   component:
    // }
  ]
});
