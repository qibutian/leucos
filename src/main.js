// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import api from "./utils/api";
import req from './utils/req'
import Vant from 'vant';
import 'vant/lib/index.css';

Vue.use(Vant);

import store from './store'
import './registerServiceWorker'
import './assets/css/common.css'
import 'babel-polyfill'
import Es6Promise from 'es6-promise'
require('es6-promise').polyfill()
Es6Promise.polyfill()

import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload)
// import {req} from '@/utils/req'
import axios from 'axios'
axios.defaults.withCredentials = true
Vue.config.productionTip = false
Vue.config.errorHandler = function (err, vm, info) {
  Raven.captureException(err)
}
Vue.prototype.$req = req
window.api = api
import * as Sentry from '@sentry/browser';
// import * as Integrations from '@sentry/integrations';

Sentry.init({
  dsn: 'http://c3339c8acff4453fb0f0ea7a23e68394@106.54.252.48:9000/2',
  // integrations: [new Integrations.Vue({
  //   Vue,
  //   attachProps: true
  // })],
});
// window.global = req
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
